; Drupal.org release file.
core = 7.x
api = 2

; Default settings
defaults[projects][subdir] = "contrib"

; Contrib modules
projects[admin_menu][version] = 3.0-rc4
projects[ctools][version] = 1.4
projects[features][version] = 2.0
projects[geocoder][version] = 1.2
projects[geocoder_autocomplete][download][type] = "git"
projects[geocoder_autocomplete][download][branch] = 7.x-1.x
projects[geofield][version] = 2.3
projects[geophp][version] = 1.7
projects[libraries][version] = 2.2
projects[link][version] = 1.2
projects[openlayers][download][branch] = 7.x-2.x
projects[openlayers][download][type] = "git"
projects[pathauto][version] = 1.2
projects[proj4js][version] = 1.2
projects[strongarm][version] = 2.0
projects[token][version] = 1.5
projects[transliteration][version] = 3.2
projects[views][version] = 3.8
projects[workbench_moderation][version] = 1.3

