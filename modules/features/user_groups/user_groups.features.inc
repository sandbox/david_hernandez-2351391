<?php
/**
 * @file
 * user_groups.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function user_groups_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function user_groups_node_info() {
  $items = array(
    'user_group' => array(
      'name' => t('User group'),
      'base' => 'node_content',
      'description' => t('Add a user group to the list'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
