<?php
/**
 * @file
 * user_group_map.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function user_group_map_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function user_group_map_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
