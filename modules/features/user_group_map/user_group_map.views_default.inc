<?php
/**
 * @file
 * user_group_map.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function user_group_map_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'user_groups';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'User groups';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'User groups';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'time';
  $handler->display->display_options['cache']['results_lifespan'] = '3600';
  $handler->display->display_options['cache']['results_lifespan_custom'] = '0';
  $handler->display->display_options['cache']['output_lifespan'] = '3600';
  $handler->display->display_options['cache']['output_lifespan_custom'] = '0';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '0';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_group_image' => 'field_group_image',
    'body' => 'body',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h2';
  /* Field: Content: Group image */
  $handler->display->display_options['fields']['field_group_image']['id'] = 'field_group_image';
  $handler->display->display_options['fields']['field_group_image']['table'] = 'field_data_field_group_image';
  $handler->display->display_options['fields']['field_group_image']['field'] = 'field_group_image';
  $handler->display->display_options['fields']['field_group_image']['label'] = '';
  $handler->display->display_options['fields']['field_group_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_group_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_group_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '300',
  );
  /* Field: Content: Location */
  $handler->display->display_options['fields']['field_location']['id'] = 'field_location';
  $handler->display->display_options['fields']['field_location']['table'] = 'field_data_field_location';
  $handler->display->display_options['fields']['field_location']['field'] = 'field_location';
  $handler->display->display_options['fields']['field_location']['element_default_classes'] = FALSE;
  /* Field: Content: GDO link */
  $handler->display->display_options['fields']['field_gdo_link']['id'] = 'field_gdo_link';
  $handler->display->display_options['fields']['field_gdo_link']['table'] = 'field_data_field_gdo_link';
  $handler->display->display_options['fields']['field_gdo_link']['field'] = 'field_gdo_link';
  $handler->display->display_options['fields']['field_gdo_link']['label'] = '';
  $handler->display->display_options['fields']['field_gdo_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_gdo_link']['alter']['text'] = '<a href="[field_gdo_link-url]">See on groups.drupal.org</a>';
  $handler->display->display_options['fields']['field_gdo_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gdo_link']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'user_group' => 'user_group',
  );
  /* Filter criterion: Content: Location (field_location) */
  $handler->display->display_options['filters']['field_location_value']['id'] = 'field_location_value';
  $handler->display->display_options['filters']['field_location_value']['table'] = 'field_data_field_location';
  $handler->display->display_options['filters']['field_location_value']['field'] = 'field_location_value';
  $handler->display->display_options['filters']['field_location_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_location_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_location_value']['expose']['operator_id'] = 'field_location_value_op';
  $handler->display->display_options['filters']['field_location_value']['expose']['label'] = 'Location';
  $handler->display->display_options['filters']['field_location_value']['expose']['description'] = 'Search for city, region or country';
  $handler->display->display_options['filters']['field_location_value']['expose']['operator'] = 'field_location_value_op';
  $handler->display->display_options['filters']['field_location_value']['expose']['identifier'] = 'location';
  $handler->display->display_options['filters']['field_location_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );

  /* Display: User group list */
  $handler = $view->new_display('page', 'User group list', 'page');
  $handler->display->display_options['path'] = 'user-groups';
  $handler->display->display_options['menu']['title'] = 'User groups';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'User groups';
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: OpenLayers Data Overlay */
  $handler = $view->new_display('openlayers', 'OpenLayers Data Overlay', 'openlayers_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_data';
  $handler->display->display_options['style_options']['data_source'] = array(
    'value' => 'wkt',
    'other_lat' => 'field_map',
    'other_lon' => 'field_map',
    'wkt' => 'field_map',
    'other_top' => 'field_map',
    'other_right' => 'field_map',
    'other_bottom' => 'field_map',
    'other_left' => 'field_map',
    'name_field' => 'title',
    'description_field' => 'body',
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_group_image' => 'field_group_image',
    'body' => 'body',
  );
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Map */
  $handler->display->display_options['fields']['field_map']['id'] = 'field_map';
  $handler->display->display_options['fields']['field_map']['table'] = 'field_data_field_map';
  $handler->display->display_options['fields']['field_map']['field'] = 'field_map';
  $handler->display->display_options['fields']['field_map']['label'] = '';
  $handler->display->display_options['fields']['field_map']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_map']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_map']['click_sort_column'] = 'geom';
  $handler->display->display_options['fields']['field_map']['settings'] = array(
    'data' => 'centroid',
  );
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['exclude'] = TRUE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_wrapper_type'] = 'h2';
  /* Field: Content: GDO link */
  $handler->display->display_options['fields']['field_gdo_link']['id'] = 'field_gdo_link';
  $handler->display->display_options['fields']['field_gdo_link']['table'] = 'field_data_field_gdo_link';
  $handler->display->display_options['fields']['field_gdo_link']['field'] = 'field_gdo_link';
  $handler->display->display_options['fields']['field_gdo_link']['label'] = '';
  $handler->display->display_options['fields']['field_gdo_link']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_gdo_link']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_gdo_link']['alter']['text'] = '<a href="[field_gdo_link-url]">See on groups.drupal.org</a>';
  $handler->display->display_options['fields']['field_gdo_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gdo_link']['click_sort_column'] = 'url';
  /* Field: Content: Group image */
  $handler->display->display_options['fields']['field_group_image']['id'] = 'field_group_image';
  $handler->display->display_options['fields']['field_group_image']['table'] = 'field_data_field_group_image';
  $handler->display->display_options['fields']['field_group_image']['field'] = 'field_group_image';
  $handler->display->display_options['fields']['field_group_image']['label'] = '';
  $handler->display->display_options['fields']['field_group_image']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_group_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_group_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_group_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  /* Field: Content: Description */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = '';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['body']['alter']['text'] = '[field_group_image][body]
[field_gdo_link]';
  $handler->display->display_options['fields']['body']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['body']['type'] = 'text_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '300',
  );

  /* Display: Map */
  $handler = $view->new_display('page', 'Map', 'page_1');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'openlayers_map';
  $handler->display->display_options['style_options']['map'] = 'user_groups_map';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['path'] = 'map';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'User groups map';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'main-menu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $export['user_groups'] = $view;

  return $export;
}
