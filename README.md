Local groups organizers website
===============================

What is this distribution?
--------------------------

This installation profile is to have a website to keep track of all the Drupal user groups all around the world. Any anonymous user can create a new group and edit any group. The groups are sent to moderation in order to be shown.

Installation and contribution
-----------------------------

### Preparing Drupal installation

On the profile root:

./scripts/build.sh [/path/to/destination]

### Install the distribution

On the distribution root (drupal/profiles/local_user_groups__organization):

./scripts/install [database]

You need to create a database first, using [database] as name, user with access and password.

